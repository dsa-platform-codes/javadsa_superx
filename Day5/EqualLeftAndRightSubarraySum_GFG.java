/*
 Equal Left and Right Subarray Sum

Given an array A of n positive numbers. The task is to find the first index in the array such that the sum of elements before it is equal to the sum of elements after it.

Note:  Array is 1-based indexed.

Example 1:

Input: 
n = 5 
A[] = {1,3,5,2,2} 
Output: 3 
Explanation: For second test case 
at position 3 elements before it 
(1+3) = elements after it (2+2). 
 

Example 2:

Input:
n = 1
A[] = {1}
Output: 1
Explanation:
Since its the only element hence
it is the only point.
Your Task:
The task is to complete the function equalSum() which takes the array and n as input parameters and returns the point. Return -1 if no such point exists.

Expected Time Complexily: O(N)
Expected Space Complexily: O(1)

Constraints:
1 <= n <= 106
1 <= A[i] <= 108
 */

public class EqualLeftAndRightSubarraySum_GFG {
    int equalSum(int [] A, int N) {
        
		int leftSum[] = new int[A.length];
        int rightSum[] = new int[A.length];
        leftSum[0] = A[0];
        rightSum[A.length-1] = A[A.length-1];
        
        for(int i = 1; i < A.length; i++){
            leftSum[i] = leftSum[i-1] + A[i];
            rightSum[A.length-1-i] = rightSum[A.length-i] + A[A.length-1-i];
        }

        // rightSum[nums.length-1] = nums[nums.length-1];
        // for(int i = nums.length-2; i >= 0; i--){
        //     rightSum[i] = rightSum[i+1] + nums[i];
        // }

        for(int i = 0; i < A.length; i++){
            if(i == 0){
                if(A.length == i+1)
                    return i+1;
                if(rightSum[i+1] == 0)
                    return i+1;
            }else if(i == A.length-1){
                if(leftSum[i-1] == 0)
                    return i+1;
            }else if(leftSum[i-1] == rightSum[i+1]){
                return i+1;
            }
        }
        return -1;
	}
}
