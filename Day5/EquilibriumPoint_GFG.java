/*
Equilibrium Point

Given an array A of n non negative numbers. The task is to find the first equilibrium point in an array. Equilibrium point in an array is an index (or position) such that the sum of all elements before that index is same as sum of elements after it.

Note: Return equilibrium point in 1-based indexing. Return -1 if no such point exists. 

Example 1:

Input: 
n = 5 
A[] = {1,3,5,2,2} 
Output: 
3 
Explanation:  
equilibrium point is at position 3 as sum of elements before it (1+3) = sum of elements after it (2+2). 
Example 2:

Input:
n = 1
A[] = {1}
Output: 
1
Explanation:
Since there's only element hence its only the equilibrium point.
Your Task:
The task is to complete the function equilibriumPoint() which takes the array and n as input parameters and returns the point of equilibrium. 

Expected Time Complexity: O(n)
Expected Auxiliary Space: O(1)

Constraints:
1 <= n <= 105
0 <= A[i] <= 109
 */
public class EquilibriumPoint_GFG {
    public static int equilibriumPoint(long nums[], int n) {

        long leftSum[] = new long[nums.length];
        long rightSum[] = new long[nums.length];
        leftSum[0] = nums[0];
        for (int i = 1; i < nums.length; i++) {
            leftSum[i] = leftSum[i - 1] + nums[i];
        }
        rightSum[nums.length - 1] = nums[nums.length - 1];
        for (int i = nums.length - 2; i >= 0; i--) {
            rightSum[i] = rightSum[i + 1] + nums[i];
        }
        for (int i = 0; i < nums.length; i++) {
            if (i == 0) {
                if (nums.length == i + 1)
                    return i + 1;
                if (rightSum[i + 1] == 0)
                    return i + 1;
            } else if (i == nums.length - 1) {
                if (leftSum[i - 1] == 0)
                    return i + 1;
            } else if (leftSum[i - 1] == rightSum[i + 1]) {
                return i + 1;
            }
        }
        return -1;
    }
}
