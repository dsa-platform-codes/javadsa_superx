import java.util.TreeMap;

class Solution {
    // Function to return the position of the first repeating element.
    public static int firstRepeated(int[] arr, int n) {
        TreeMap<Integer,Integer> tm = new TreeMap<Integer,Integer>();
        int res = Integer.MAX_VALUE;
        for(int i=0;i<n;i++){
           int ele = arr[i];
           
           if(!tm.containsKey(ele)){
               tm.put(ele,i);
           }
           
           else {
             int min =  tm.get(ele);
             res = Math.min(res,min);
           }
           
        }
        
        if(res==Integer.MAX_VALUE){
            return -1;
        }
        else {
            return res+1;
        }
    }
    public static void main(String[] args) {
        int arr[] = new int[]{1, 5, 3, 4, 3, 5, 6};
        System.out.println(firstRepeated(arr,arr.length));
    }
}