import java.util.Arrays;

public class KeyPair_GFG {
    boolean hasArrayTwoCandidates(int arr[], int n, int x) {
        Arrays.sort(arr);
        int start = 0;
        int end = n - 1;

        while (start < end) {
            int sum = arr[start] + arr[end];
            if (sum == x) {
                return true;
            } else if (sum < x) {
                start++;
            } else if (sum > x) {
                end--;
            }
        }
        return false;
    }
}
