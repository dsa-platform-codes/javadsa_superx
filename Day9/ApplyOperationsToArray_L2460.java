import java.util.Arrays;

public class ApplyOperationsToArray_L2460 {
    public static int[] applyOperations(int[] nums) {
        int idx = 0;
        for (int i = 0; i < nums.length; i++) {
            if (i == nums.length - 1) {
                if (nums[i] == 0)
                    break;
            } else if (nums[i] == nums[i + 1]) {
                nums[i] = nums[i] * 2;
                nums[i + 1] = 0;
            }
            if (nums[i] != 0) {
                int temp = nums[i];
                nums[i] = nums[idx];
                nums[idx] = temp;
                idx++;
            }
        }
        return nums;
    }

    public static void main(String[] args) {
        int arr[] = new int[] { 1, 2, 2, 1, 1, 0 };
        applyOperations(arr);
        System.out.println(Arrays.toString(arr));
    }
}
