
class MissingNumber_L268{
    public static int missingNumber(int[] nums) {
        int sum = 0;
        for(int i = 0; i < nums.length; i++){
            sum = sum + nums[i];
        }
        return (nums.length*(nums.length+1)/2)-sum;
    }
    public static void main(String[] args) {
        int arr[] = new int[]{9,6,4,2,3,5,7,0,1};
        System.out.println(missingNumber(arr));
    }
}
