public class BuildingReceivingSunlight_GFG {
    public static int longest(int arr[], int n) {
        int count = 1;
        for (int i = 1; i < arr.length; i++) {
            int ele = arr[i];
            if (arr[i] <= arr[i - 1])
                arr[i] = arr[i - 1];
            if (ele >= arr[i - 1])
                count++;
        }
        return count;
    }

    public static void main(String[] args) {
        int arr[] = new int[] { 6, 2, 8, 4, 11, 13 };
        System.out.println(longest(arr, arr.length));
    }
}
