/*
 Problem statement
The Ninja squad aims to find 3 ninjas who are great collectively. Each ninja has his or her ability expressed as an integer. 3 candidates are great collectively if the product of their abilities is maximum. Given the abilities of ‘N’ ninjas in an array ARR[], find the maximum collective ability from the given pool of ninjas.

For Example :

Given N = 5,
and ARR[] = {1, 3, 5, 4, 2}
Therefore, we can see that three ninjas with ability 3,4 and 5 will give us the maximum ability and will be called great therefore the output will be 60. 
Detailed explanation ( Input/output format, Notes, Images )
Sample Input 1 :
2
5
1 3 2 4 5
3
1 4 5
Sample Output 1 :
60
20
Explanation of Sample Input 1 :
For the first test case: the ninjas chosen are 2nd 4th and 5th because they will give us the maximum product.Product of abilities 3 * 4 * 5 = 60. Hence the answer is 60.

For the second test case: the 3 ninjas are chosen a whose product of abilities 1*4*5 = 20. Hence the answer is 20.
Sample Input 2 :
2
4
4 4 4 4
5
1 1 1 1 2    
Sample Output 2 :
64
2    
 */

import java.util.ArrayList;
import java.util.Collections;

public class ThreeNinjaCandidate_CodingNinja {
    public static int ninjaCandidate(ArrayList<Integer> arr) {
		int n = arr.size();

		Collections.sort(arr);

		int takingLastThree = arr.get(n - 3) * arr.get(n - 2) * arr.get(n - 1);
		int takingTwoFromBegin = arr.get(0) * arr.get(1) * arr.get(n - 1);

		return Math.max(takingLastThree, takingTwoFromBegin);
	}
}
