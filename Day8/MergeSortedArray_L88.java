import java.util.Arrays;

class Solution {
    public static void merge(int[] nums1, int m, int[] nums2, int n) {
        int arr[] = new int[m];
        for(int i = 0; i < m; i++){
            arr[i] = nums1[i];
        }

        int i = 0, j = 0, k = 0;
        while(i < m && j < n){
            if(arr[i] <= nums2[j]){
                nums1[k] = arr[i];
                i++;
            }else{
                nums1[k] = nums2[j];
                j++;
            }
            k++;
        }
        while(i < m){
            nums1[k] = arr[i];
            i++;
            k++;
        }
        while(j < n){
            nums1[k] = nums2[j];
            j++;
            k++;
        }
    }
    public static void main(String[] args) {
        int arr1[] = new int[]{1,2,3,0,0,0};
        int arr2[] = new int[]{2,5,6};
        merge(arr1, 3, arr2, 3);
        Arrays.toString(arr1);
    }
}