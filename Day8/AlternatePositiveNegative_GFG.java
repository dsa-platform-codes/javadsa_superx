import java.util.ArrayList;
import java.util.List;

public class AlternatePositiveNegative_GFG {
    void rearrange(int arr[], int n) {
        List<Integer> posArr = new ArrayList<Integer>();
        List<Integer> negArr = new ArrayList<Integer>();
        for (int i = 0; i < n; i++) {
            if (arr[i] < 0)
                negArr.add(arr[i]);
            else
                posArr.add(arr[i]);
        }
        int k = 0, i = 0, j = 0;
        while (i < posArr.size() && j < negArr.size()) {
            if (k % 2 == 0) {
                arr[k] = posArr.get(i);
                i++;
            } else {
                arr[k] = negArr.get(j);
                j++;
            }
            k++;
        }
        while (i < posArr.size()) {
            arr[k] = posArr.get(i);
            i++;
            k++;
        }
        while (j < negArr.size()) {
            arr[k] = negArr.get(j);
            j++;
            k++;
        }
    }

    public static void main(String[] args) {

    }
}
