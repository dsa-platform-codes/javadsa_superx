import java.util.HashMap;

public class CountPairsWithGivenSum_GFG {
     static int getPairsCount(int[] arr, int n, int k) {
       HashMap<Integer,Integer> map = new HashMap<>();
       int count = 0;
       for(int a : arr){
            count+=map.getOrDefault(k-a,0);
            map.put(a,map.getOrDefault(a,0)+1);
       }
       return count;
    }
    public static void main(String[] args) {
        int arr[] = new int[]{1, 5, 7, 1};
        System.out.println(getPairsCount(arr, arr.length,6));
    }
}
