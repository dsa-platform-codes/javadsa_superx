import java.util.Arrays;

class Solution {
    public static int majorityElement(int[] nums) {
        int count = 0;
        int maxCnt = Integer.MIN_VALUE;
        int ans = -1;
        Arrays.sort(nums);
        for (int i = 0; i < nums.length - 1; i++) {
            if (nums[i] == nums[i + 1]) {
                count++;
            } else {
                if (count > maxCnt) {
                    ans = nums[i];
                    maxCnt = count;
                }
                count = 0;
            }
        }
        if (count > maxCnt) {
            ans = nums[nums.length - 1];
            maxCnt = count;
        }
        return ans;
    }

    public static void main(String[] args) {
        int arr[] = new int[]{2,2,1,1,1,2,2};
        System.out.println(majorityElement(arr));
    }
}
