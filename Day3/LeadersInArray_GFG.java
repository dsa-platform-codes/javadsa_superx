import java.util.ArrayList;
import java.util.Collections;

public class LeadersInArray_GFG {
    static ArrayList<Integer> leaders(int arr[], int n){
        ArrayList<Integer> ans = new  ArrayList<Integer>();
         int j = 0;
        for(int i = n - 1; i >= 0; i-- ){
            
            if (arr[i] >= j){
                ans.add(arr[i]);
                j = arr[i];
            }
            
        }
        Collections.reverse(ans);
        return ans;
    }
    public static void main(String[] args) {
        System.out.println(leaders(new int[]{16,17,4,3,5,2},6));
    }
}
