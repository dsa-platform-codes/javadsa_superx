import java.util.Arrays;
import java.util.HashMap;

class Solution {
    public static int[] twoSum(int[] nums, int target) {
        HashMap<Integer, Integer> hm = new HashMap<Integer, Integer>();
        for(int i = 0; i < nums.length; i++){
            hm.put(nums[i], i);
        }

        for(int i = 0; i < nums.length; i++){
            if(hm.containsKey(target-nums[i]) && hm.get(target-nums[i]) != i){
                return new int[]{i, hm.get(target-nums[i])};
            }
        }
        return new int[]{};
    }
    public static void main(String[] args) {
        int arr[] = new int[]{2,7,11,15};
        System.err.println(Arrays.toString(twoSum(arr, 9)));
    }
}
