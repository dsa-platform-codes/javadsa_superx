import java.util.Arrays;

public class SmallestAndSecondSmallest_GFG {
    public static long[] minAnd2ndMin(long a[], long n) {
        long min = Long.MAX_VALUE;
        long sMin = Long.MAX_VALUE;
        long arr[] = new long[2];

        for (int i = 0; i < n; i++) {
            if (a[i] < min) {
                sMin = min;
                min = a[i];
            }
            if (a[i] > min && a[i] < sMin) {
                sMin = a[i];
            }
        }
        if (min == Long.MAX_VALUE || sMin == Long.MAX_VALUE) {
            arr[0] = -1;
            arr[1] = -1;
        } else {
            arr[0] = min;
            arr[1] = sMin;
        }

        return arr;
    }

    public static void main(String[] args) {
        long arr[] = new long[] { 2, 4, 3, 5, 6 };
        System.out.println(Arrays.toString(minAnd2ndMin(arr, arr.length)));
    }
}
